## Enum Imports
using module .\Enums\LogLevel.psm1

## Class Imports

## Modules Imports
using module .\Modules\Logging.psm1

## Variables
$Script:LogPath = "$((Get-Location).Path)\Logs\Log.txt"

## Logic
try {
    New-Log `
        -Severity ([LogLevel]::Info) `
        -LogPath $Script:LogPath `
        -Message "Starting Entra ID Role Sieve" 

} catch {
    Write-Error $_

} finally {
    Remove-Module -Name LogLevel
    Remove-Module -Name Logging
}

