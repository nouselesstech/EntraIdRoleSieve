using module ..\Enums\LogLevel.psm1
Function New-Log {
    param(
        [Parameter(Mandatory=$True)]
        [LogLevel]$Severity,

        [Parameter(Mandatory=$True)]
        [string]$Message,

        [Parameter(Mandatory=$True)]
        [string]$LogPath
    )

    try {
        $Date = Get-Date -UFormat "%Y%m%dT%T"
        $Log = "$Date`: $Severity`: $Message"
        $Log | Out-File -Append -Path $LogPath

        Switch($Severity) {
            ([LogLevel]::Warn)  { Write-Warning     $Message}
            ([LogLevel]::Err)   { Write-Error       $Message}
            ([LogLevel]::Debug) { Write-Debug       $Message}
            ([LogLevel]::Info)  { Write-Information $Message -InformationAction Continue}
            Default             { Write-Information $Message -InformationAction Continue}
        }

    } catch {
        throw "Unable to write logs! `r`n $_"
    }
}
